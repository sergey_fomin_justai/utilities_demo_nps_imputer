package com.justai.jaicf.npsAfterCompletedRequestBot.model

import com.justai.jaicf.context.BotContext
import com.justai.jaicf.npsAfterCompletedRequestBot.JSON
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromJsonElement

class User(private val context: BotContext) {
    var phoneNumber: String? by context.client
    var request: Request? by context.client
    var gmtZone: String? by context.client

    fun clear() {
        request?.clear()
        phoneNumber = null
        request = null
    }

    fun isRequestEmpty() = request == null || request?.isEmpty() == true

    fun setDataFromPayload(payload: JsonObject?) {
        gmtZone = payload?.get("GMT_ZONE")?.let { JSON.decodeFromJsonElement<String>(it) }
        phoneNumber = payload?.get("phone")?.let { JSON.decodeFromJsonElement<String>(it)}
        request = makeRequestObjectFromPayload(payload)
    }

    private fun makeRequestObjectFromPayload(payload: JsonObject?): Request {
        val req = Request(context)
        req.id = payload?.get("requestId")?.let { JSON.decodeFromJsonElement<String>(it) }
        req.requestName = payload?.get("requestName")?.let { JSON.decodeFromJsonElement<String>(it) }
        req.requestStatus = payload?.get("requestStatus")?.let { JSON.decodeFromJsonElement<String>(it) }
        return req
    }
}


