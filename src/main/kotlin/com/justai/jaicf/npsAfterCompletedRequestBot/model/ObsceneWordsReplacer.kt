package com.justai.jaicf.npsAfterCompletedRequestBot.model

import com.justai.jaicf.npsAfterCompletedRequestBot.JSON
import com.justai.jaicf.npsAfterCompletedRequestBot.integration.ExternalConnector
import com.justai.jaicf.npsAfterCompletedRequestBot.toJsonElement
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.decodeFromJsonElement
import kotlin.random.Random

object ObsceneWordsReplacer {

    fun replace(text: String?): String? {
        if (text.isNullOrBlank()) { return text }

        val jsonText = JsonObject(mapOf( "text" to text.toJsonElement()))

        // The service returns object with lists of indexes. "bad" list contains indexes of obscene words
        val obsceneWordsJsonObject = ExternalConnector.detectObsceneWords(jsonText)?.get("bad")

        val obsceneWords = if (obsceneWordsJsonObject != null) {
            JSON.decodeFromJsonElement<List<Int>>(obsceneWordsJsonObject)
        } else { listOf() }

        val textToList = text.split(" ").toMutableList()
        obsceneWords.forEach { index ->
            val obsceneWord = textToList[index]
            textToList[index] = replacer(obsceneWord.length)
        }
        return textToList.joinToString(" ")
    }

    private fun replacer(length: Int): String {
        val charPool: List<Char> = listOf('*', '#', '$', '@')
        return (1..length + 1)
            .map { Random.nextInt(0, charPool.size) }
            .map(charPool::get)
            .joinToString("")
    }
}