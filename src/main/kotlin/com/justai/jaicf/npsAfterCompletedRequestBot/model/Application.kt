package com.justai.jaicf.npsAfterCompletedRequestBot.model

import com.justai.jaicf.context.BotContext

class Application(val context: BotContext) {

    val catchAll = CatchAllContext()
    val askOperator = AskOperatorContext()

    /** This flag is used to check if the scenario is already finished.
     *
     * In fact, it's a double check for cases when user hangs up right before bot ends session,
     * and when this happens bot sends all the reports and finishes the dialogue twice.
     *
     * When [app.clear()] is called this is set to true. */
    var isFinished: Boolean by SessionProperty(context, false)

    fun clear() {
        catchAll.counter = 0
        askOperator.counter = 0
        isFinished = true
    }

    inner class CatchAllContext {
        var counter: Int by SessionProperty(context, 0)
    }

    inner class AskOperatorContext {
        var counter: Int by SessionProperty(context, 0)
    }
}