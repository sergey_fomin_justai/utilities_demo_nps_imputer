package com.justai.jaicf.npsAfterCompletedRequestBot.model

import com.justai.jaicf.context.BotContext
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class SessionProperty<C,T>(var ctx: BotContext, var default: T): ReadWriteProperty<C, T> {

    override fun getValue(thisRef: C, property: KProperty<*>): T {
        return ctx.session[property.name] as? T ?: return default.also { ctx.session[property.name] = default }
    }

    override fun setValue(thisRef: C, property: KProperty<*>, value: T) {
        ctx.session[property.name] = value
    }
}