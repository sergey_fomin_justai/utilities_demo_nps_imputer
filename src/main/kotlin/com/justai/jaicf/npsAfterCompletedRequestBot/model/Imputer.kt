package com.justai.jaicf.npsAfterCompletedRequestBot.model



import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.HttpClients
import java.time.LocalTime
import java.time.OffsetDateTime
import java.time.ZoneId
import java.util.*


object Imputer {
    const val imputedUrl = "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?"
    const val preRecordedUrl = "https://248305.selcdn.ru/demo_bot_static/utilities-NPS"
    const val token = "KaiPhei7rae1eeh"


    fun getGreetingPhrase(gmtZone: String? = null, companyName: String): String {
        val utcTime = LocalTime.now(TimeZone.getTimeZone("UTC").toZoneId())

        val time = if (gmtZone == null) {
            utcTime.hour + 3        // For Moscow timezone
        } else {
            val zoneId = ZoneId.of(gmtZone)
            utcTime.hour + OffsetDateTime.now(zoneId).offset.totalSeconds / 3600
        }
        return when (time) {
            in 4..11 -> {
                cache("https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?company=${companyName.replace("\"", "").replace(" ", "%20")}&template_id=GREETING_good_morning_company_may_i_ask&speed=0.9&token=$token")
                "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?company=${companyName.replace("\"", "").replace(" ", "%20")}&template_id=GREETING_good_morning_company_may_i_ask&speed=0.9&token=$token"
            }
            in 12..16 -> {
                cache("https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?company=${companyName.replace("\"", "").replace(" ", "%20")}&template_id=GREETING_good_afternoon_company_may_i_ask&speed=0.9&token=$token")
                "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?company=${companyName.replace("\"", "").replace(" ", "%20")}&template_id=GREETING_good_afternoon_company_may_i_ask&speed=0.9&token=$token"
            }
            in 17..24 -> {
                cache("https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?company=${companyName.replace("\"", "").replace(" ", "%20")}&template_id=GREETING_good_evening_company_may_i_ask&speed=0.9&token=$token")
                "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?company=${companyName.replace("\"", "").replace(" ", "%20")}&template_id=GREETING_good_evening_company_may_i_ask&speed=0.9&token=$token"
            }
            else -> {
                cache("https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?company=${companyName.replace("\"", "").replace(" ", "%20")}&template_id=GREETING_good_afternoon_company_may_i_ask&speed=0.9&token=$token")
                "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?company=${companyName.replace("\"", "").replace(" ", "%20")}&template_id=GREETING_good_afternoon_company_may_i_ask&speed=0.9&token=$token"
            }
        }
    }



    fun cache(url: String) {
        HttpClients.createDefault().let {
            val httpPost = HttpPost(url)
            it.execute(httpPost)
            it.close()
        }
    }

    fun cache(urls: List<String>) {
        for (url in urls) {
            HttpClients.createDefault().let {
                val httpPost = HttpPost(url.replace(" ", "%20"))
                it.execute(httpPost)
                it.close()
            }
        }
    }
}