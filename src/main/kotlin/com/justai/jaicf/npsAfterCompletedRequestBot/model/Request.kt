package com.justai.jaicf.npsAfterCompletedRequestBot.model

import com.justai.jaicf.context.BotContext


class Request(ctx: BotContext) {
    var id: String? by SessionProperty(ctx, null)
    var requestName: String? by SessionProperty(ctx, null)
    var requestStatus: String? by SessionProperty(ctx, null)
    var evaluation: Int? by SessionProperty(ctx, null)
    var commentary: String? by SessionProperty(ctx, null)


    fun clear() {
        requestName = null
        requestStatus = null
        evaluation = null
        commentary = null
    }

    fun setComment(input: String = "") {
        val default = "Жилец отказался давать комментарии"

        val comment = when(input) {
            "" -> { default }
            "speechNotRecognized" -> { "" }
            else -> { input }
        }
        if (commentary.isNullOrBlank()) {
            commentary = comment
        } else {
            if (commentary != default) {
                commentary += "; $comment"
            }
        }
    }

    fun isEmpty() = id.isNullOrBlank() || requestName.isNullOrBlank()

    fun getMapReport(): Map<String, String?> {
        return mapOf(
            "Id" to id,
            "Заявка" to requestName,
            "Cтатус" to requestStatus,
            "Оценка" to evaluation.toString(),
            "Комментарий" to ObsceneWordsReplacer.replace(commentary)
        )
    }

    fun getStringReport(): String {
        return "Заявка: ${requestName ?: ""}; \n" +
                "Cтатус: ${requestStatus ?: ""}; \n " +
                "Оценка: ${evaluation ?: ""}; \n" +
                (if (commentary.isNullOrBlank()) "Комментарии отсутствуют;"
                else "Комментарии: ${commentary ?: ""}")
    }

    fun isEvaluationEmpty(): Boolean {
        return evaluation == null &&
                (commentary?.contains(Regex(pattern = "Жилец отказался давать оценку")) == false || commentary == null)
    }
}