package com.justai.jaicf.npsAfterCompletedRequestBot.model

import com.justai.jaicf.context.BotContext
import com.justai.jaicf.npsAfterCompletedRequestBot.integration.ExternalConnector
import com.justai.jaicf.npsAfterCompletedRequestBot.toJsonElement
import kotlinx.serialization.json.JsonObject


class CallResult(private val context: BotContext, usr: User) {
    var startCallStatus: String? by SessionProperty(context, null)
    var endCallStatus: String? by SessionProperty(context, null)
    var callResultUrl: String? by SessionProperty(context, null)
    var user: User? by SessionProperty(context, usr)

    fun clear() {
        startCallStatus = null
        callResultUrl = null
        user = null
    }

    fun sendReport() {
        ExternalConnector.sendResult(getJsonResult())
    }

    private fun getJsonResult(): JsonObject {

        val resultMap = mapOf(
            "phoneNumber" to user?.phoneNumber.toJsonElement(),
            "requestName" to user?.request?.requestName.toJsonElement(),
            "requestStatus" to user?.request?.requestStatus.toJsonElement(),
            "requestId" to user?.request?.id.toJsonElement(),
            "evaluation" to user?.request?.evaluation.toString().toJsonElement(),
            "comment" to ObsceneWordsReplacer.replace(user?.request?.commentary).toJsonElement(),
            "callResult" to startCallStatus.toJsonElement(),
            "callRecordUrl" to callResultUrl.toJsonElement()
        )
        return JsonObject(resultMap)
    }
}




