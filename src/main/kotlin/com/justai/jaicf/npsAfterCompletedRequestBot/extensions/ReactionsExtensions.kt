package com.justai.jaicf.npsAfterCompletedRequestBot.extensions

import com.justai.jaicf.channel.jaicp.reactions.TelephonyReactions
import com.justai.jaicf.context.BotContext
import com.justai.jaicf.npsAfterCompletedRequestBot.model.Application
import com.justai.jaicf.npsAfterCompletedRequestBot.model.CallResult
import com.justai.jaicf.npsAfterCompletedRequestBot.model.User
import com.justai.jaicf.npsAfterCompletedRequestBot.scenarios.consts.SUCCESS
import com.justai.jaicf.reactions.Reactions
import kotlin.collections.component1
import kotlin.collections.component2
import kotlin.collections.set
import kotlin.random.Random


fun TelephonyReactions.audio(
    mainPhrase: String,
    vararg onCallbackPhrases: String,
    bargeIn: Boolean = false,
    bargeInContext: String? = null
) {
    val toSay = if (botContext.client["fromOfftop"] == true) {
        onCallbackPhrases.asList()[smartRandom(onCallbackPhrases.size, botContext) % onCallbackPhrases.size]
    } else {
        mainPhrase
    }

    if (bargeInContext != null) {
        audio(toSay, bargeInContext = bargeInContext)
    } else {
        audio(toSay, bargeIn = bargeIn)
    }
}


fun TelephonyReactions.say(phrase: String, vararg onCallbackPhrases: String, bargeIn: Boolean = false, bargeInContext: String? = null) {
    val toSay = if (botContext.client["fromOfftop"] == true) {
        onCallbackPhrases.asList()[smartRandom(onCallbackPhrases.size, botContext) % onCallbackPhrases.size]
    } else {
        phrase
    }
    if (bargeInContext != null) {
        say(toSay, bargeInContext = bargeInContext)
    } else {
        say(toSay, bargeIn = bargeIn)
    }
}


fun Reactions.say(phrase: String, vararg onCallbackPhrases: String, bargeIn: Boolean = false, bargeInContext: String? = null) {
    val toSay = if (botContext.client["fromOfftop"] == true) {
        onCallbackPhrases.asList()[smartRandom(onCallbackPhrases.size, botContext) % onCallbackPhrases.size]
    } else {
        phrase
    }
    if (bargeInContext != null) {
        say(toSay, bargeInContext = bargeInContext)
    } else {
        say(toSay, bargeIn = bargeIn)
    }
}

fun TelephonyReactions.makeReport(result: CallResult, payload: String) {
    setResult(result.endCallStatus ?: SUCCESS, payload)
    report("Результат звонка", result.endCallStatus ?: "")
    report("Статус дозвона", result.startCallStatus ?: "")
    report("Ссылка на запись диалога", result.callResultUrl.toString())
    val rep = result.user?.request?.getMapReport()
    rep?.forEach { (key, value) -> report(key, value ?: "") }
}


fun TelephonyReactions.hangupAndClear(app: Application, result: CallResult, user: User) {
    hangup()
    result.clear()
    user.clear()
    app.clear()
}

/** Changes the state to the previous one and
 * executes its action block immediately.
 * @param fromOfftop specifies whether current state is offtop
 * (not the part of the main scenario flow).
 * @param callBackState optional callBackState in case if there's no last state in the context.
 * */
fun Reactions.goToPreviousState(
    fromOfftop: Boolean = true,
    callBackState: String = "/You've had a request, close?"
) {
    val lastState = botContext.dialogContext.backStateStack.peekFirst()
    botContext.client["fromOfftop"] = fromOfftop
    if (lastState != null) {
        go(lastState)
    } else {
        go(callBackState)
    }
}

fun Reactions.changeStateToPrevious(
    fromOfftop: Boolean = true,
    callBackState: String = "/You've had a request, close?"
) {
    val lastState = botContext.dialogContext.backStateStack.peekFirst()
    botContext.client["fromOfftop"] = fromOfftop
    if (lastState != null) {
        changeState(lastState)
    } else {
        changeState(callBackState)
    }

}

fun smartRandom(max: Int, bc: BotContext): Int {
    val id = "${bc.dialogContext.currentState}_$max"
    var smartRandom: MutableMap<String, MutableList<Int>>? by bc.session

    if (smartRandom == null) {
        smartRandom = mutableMapOf()
    }

    var prev = smartRandom!!.getOrDefault(id, mutableListOf())

    var i = 0
    var ic = 0
    while (ic < max * 5) {
        ic++
        i = Random.nextInt(ic)
        if (prev.indexOf(i) == -1) {
            break
        }
    }

    prev.add(i)

    if (prev.size > max / 2) {
        prev = prev.subList(1, prev.size).toMutableList()
    }

    smartRandom!![id] = prev

    return i
}