package com.justai.jaicf.npsAfterCompletedRequestBot.extensions

import com.justai.jaicf.channel.jaicp.dto.TelephonyBotRequest
import com.justai.jaicf.channel.jaicp.dto.jaicpAnalytics
import com.justai.jaicf.channel.jaicp.reactions.TelephonyReactions
import com.justai.jaicf.context.ActionContext
import com.justai.jaicf.context.ActivatorContext
import com.justai.jaicf.context.DefaultActionContext
import com.justai.jaicf.npsAfterCompletedRequestBot.model.Application
import com.justai.jaicf.npsAfterCompletedRequestBot.model.CallResult
import com.justai.jaicf.npsAfterCompletedRequestBot.model.User

val ActionContext<ActivatorContext, TelephonyBotRequest, TelephonyReactions>.companyName: String
    get() = request.calleePayload?.get("companyName")?.toString() ?: "Управляющая компания Джаст Эй Ай"

val DefaultActionContext.app: Application
    get() = Application(context)

val DefaultActionContext.user: User
    get() = User(context)

val DefaultActionContext.callResult: CallResult
    get() = CallResult(context, user)

val DefaultActionContext.sessionId: String?
    get() {
        val s = context.session["com/justai/jaicf/jaicp/logging/conversationSession/sessionManager"] as String?
        return s?.replace("{\"sessionId\":\"", "")?.replace("\"}", "")
}

fun DefaultActionContext.saveAsLastState(fromOfftop: Boolean = false) {
    context.dialogContext.backStateStack.push(context.dialogContext.currentState)
    context.client["fromOfftop"] = fromOfftop
}

fun DefaultActionContext.setJaicpAnalytics() {
    callResult.startCallStatus?.let { jaicpAnalytics.setSessionData("Статус дозвона", it) }
    callResult.user?.request?.requestStatus?.let { jaicpAnalytics.setSessionData("Статус заявки", it) }
    callResult.endCallStatus?.let { jaicpAnalytics.setSessionResult(it) }
}
