package com.justai.jaicf.npsAfterCompletedRequestBot.extensions

import com.justai.jaicf.channel.jaicp.dto.TelephonyBotRequest
import com.justai.jaicf.npsAfterCompletedRequestBot.JSON
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.decodeFromJsonElement
import kotlinx.serialization.json.jsonObject

@Serializable
data class JaicpData(
    var resterisk: ResteriskData? = null
)

@Serializable
data class ResteriskData(
    var channel: String? = null,
    var callRecordsDownloadData: CallRecordsDownloadData? = null,
    var incoming: Boolean? = null
)

@Serializable
data class CallRecordsDownloadData(
    var audioToken: AudioTokenData? = null,
    var downloadUrl: String? = null
)

@Serializable
data class AudioTokenData(
    var token: String? = null,
)

@Serializable
data class CallAttemtsHistory(
    var completedAttempts: Int,
    var completedAttemptsToPhone: Int,
    var availableAttempts: Int,
    var availableAttemptsToPhone: Int
)


fun TelephonyBotRequest.getAudioUrl(sessionId: String): String? {
    val jacpData = this.jaicp.data?.let { JSON.decodeFromJsonElement<JaicpData>(it) }
    val url = jacpData?.resterisk?.callRecordsDownloadData?.downloadUrl
    return url?.replace("{sessionId}", sessionId)
}

val TelephonyBotRequest.callAttemtsHistory: CallAttemtsHistory?
    get() = this.jaicp.rawRequest.jsonObject["originateData"]?.jsonObject?.get("callScenarioData")
            ?.jsonObject?.get("callAttemptsHistory")?.let { JSON.decodeFromJsonElement(it) }
