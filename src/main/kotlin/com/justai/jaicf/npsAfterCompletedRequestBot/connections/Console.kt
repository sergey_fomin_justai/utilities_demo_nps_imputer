package com.justai.jaicf.npsAfterCompletedRequestBot.connections

import com.justai.jaicf.channel.ConsoleChannel
import com.justai.jaicf.npsAfterCompletedRequestBot.npsBot

fun main() {
    ConsoleChannel(npsBot).run("/start")
}