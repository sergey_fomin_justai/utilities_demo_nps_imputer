package com.justai.jaicf.npsAfterCompletedRequestBot.connections

import com.justai.jaicf.channel.jaicp.JaicpServer
import com.justai.jaicf.channel.jaicp.channels.ChatApiChannel
import com.justai.jaicf.channel.jaicp.channels.ChatWidgetChannel
import com.justai.jaicf.channel.jaicp.channels.TelephonyChannel
import com.justai.jaicf.npsAfterCompletedRequestBot.accessToken
import com.justai.jaicf.npsAfterCompletedRequestBot.npsBot

fun main() {
    JaicpServer(
        botApi = npsBot,
        accessToken = accessToken,
        channels = listOf(
            ChatApiChannel,
            ChatWidgetChannel,
            TelephonyChannel
        ),
        port = 8080
    ).start(wait = true)
}