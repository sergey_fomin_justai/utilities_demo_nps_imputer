package com.justai.jaicf.npsAfterCompletedRequestBot.connections

import com.justai.jaicf.channel.jaicp.JaicpPollingConnector
import com.justai.jaicf.channel.jaicp.channels.ChatApiChannel
import com.justai.jaicf.channel.jaicp.channels.ChatWidgetChannel
import com.justai.jaicf.channel.jaicp.channels.TelephonyChannel
import com.justai.jaicf.npsAfterCompletedRequestBot.accessToken
import com.justai.jaicf.npsAfterCompletedRequestBot.npsBot
import io.ktor.client.features.logging.*

fun main() {
    JaicpPollingConnector(
        npsBot,
        accessToken,
        channels = listOf(
            ChatApiChannel,
            ChatWidgetChannel,
            TelephonyChannel
        ),
        logLevel = LogLevel.BODY
    ).runBlocking()
}