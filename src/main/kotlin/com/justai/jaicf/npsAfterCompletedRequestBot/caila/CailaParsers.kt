package com.justai.jaicf.npsAfterCompletedRequestBot.caila

import com.justai.jaicf.activator.caila.CailaIntentActivatorContext


abstract class CailaEntityParser(private val ctx: CailaIntentActivatorContext) {

    fun getSlot(key: String?): String? = ctx.slots[key]
    fun getEntity(key: String): String? = ctx.entities.firstOrNull {it.entity == key}?.value

}

class CailaNumberParser(ctx: CailaIntentActivatorContext): CailaEntityParser(ctx) {
    fun getNumber(): Double? {
        return getEntity("zb.number")?.toDouble()
    }
}



