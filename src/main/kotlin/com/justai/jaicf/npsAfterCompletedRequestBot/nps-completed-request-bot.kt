package com.justai.jaicf.npsAfterCompletedRequestBot

import com.justai.jaicf.BotEngine
import com.justai.jaicf.activator.caila.CailaIntentActivator
import com.justai.jaicf.activator.caila.CailaNLUSettings
import com.justai.jaicf.activator.catchall.CatchAllActivator
import com.justai.jaicf.activator.event.BaseEventActivator
import com.justai.jaicf.activator.regex.RegexActivator
import com.justai.jaicf.builder.append
import com.justai.jaicf.channel.jaicp.logging.JaicpConversationLogger
import com.justai.jaicf.hook.AfterActionHook
import com.justai.jaicf.logging.AudioReaction
import com.justai.jaicf.logging.Slf4jConversationLogger
import com.justai.jaicf.npsAfterCompletedRequestBot.scenarios.commonStates
import com.justai.jaicf.npsAfterCompletedRequestBot.scenarios.mainScenario
import com.justai.jaicf.npsAfterCompletedRequestBot.scenarios.offtopStates
import java.util.*

val accessToken: String = System.getenv("JAICP_API_TOKEN") ?: Properties().run {
    load(CailaNLUSettings::class.java.getResourceAsStream("/jaicp.properties"))
    getProperty("apiToken")
}

val imputerToken: String = System.getenv("JAICP_API_TOKEN") ?: Properties().run {
    load(CailaNLUSettings::class.java.getResourceAsStream("/jaicp.properties"))
    getProperty("imputerToken")
}

private val cailaNLUSettings = CailaNLUSettings(
    accessToken = accessToken,
    confidenceThreshold = 0.45
)


val npsBot = BotEngine(
    scenario =  mainScenario append offtopStates append commonStates,
    conversationLoggers = arrayOf(
        JaicpConversationLogger(accessToken),
        Slf4jConversationLogger()
    ),
    activators = arrayOf(
        BaseEventActivator,
        CailaIntentActivator.Factory(cailaNLUSettings),
        RegexActivator,
        CatchAllActivator
    )
).apply {
    hooks.addHookAction<AfterActionHook> {
        context.session["lastPhrase"] = (reactions.executionContext.reactions.last() as AudioReaction).audioUrl
    }
}
