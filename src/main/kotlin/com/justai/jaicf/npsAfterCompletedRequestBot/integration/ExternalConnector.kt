package com.justai.jaicf.npsAfterCompletedRequestBot.integration

import com.justai.jaicf.helpers.logging.WithLogger
import io.ktor.client.*
import io.ktor.client.features.json.*
import io.ktor.client.features.json.serializer.*
import io.ktor.client.features.logging.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.json.JsonObject

object ExternalConnector: WithLogger {
    private const val urlDS = "https://ds24.ru/api/api/justai/justai"
    private const val urlBasenji = "https://basenji-abc.com/parser"

    private val client = HttpClient {
        expectSuccess = true
        install(Logging) {
            logger = Logger.DEFAULT
            level = LogLevel.BODY
        }
        install(JsonFeature) {
            serializer = KotlinxSerializer()
        }
    }

    fun detectObsceneWords(text: JsonObject): JsonObject? {
        var httpResponse: JsonObject? = null
        try {
            runBlocking {
                httpResponse = client.post<JsonObject>(urlBasenji) {
                    contentType(ContentType.Application.Json)
                    header(HttpHeaders.Authorization, "Token 76309566-2b37-4642-9b83-1ec2095aac37")
                    body = text
                }
            }
        } catch (ex: Exception) {
            ExternalConnector.logger.warn("Failed to detect obscene words", ex)
        }
        return httpResponse
    }

    fun sendResult(result: JsonObject): String? {
        try {
            return runBlocking {
                client.post<String>(urlDS) {
                    contentType(ContentType.Application.Json)
                    accept(ContentType.Text.Plain)
                    body = result
                }
            }
        } catch (ex: Exception) {
            ExternalConnector.logger.warn("Failed to send result", ex)
        }
        return null
    }
}