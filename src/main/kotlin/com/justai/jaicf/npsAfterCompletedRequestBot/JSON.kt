package com.justai.jaicf.npsAfterCompletedRequestBot

import kotlinx.serialization.json.Json
import kotlinx.serialization.json.encodeToJsonElement

val JSON = Json { ignoreUnknownKeys = true; isLenient = true }

fun String?.toJsonElement() = JSON.encodeToJsonElement(this)