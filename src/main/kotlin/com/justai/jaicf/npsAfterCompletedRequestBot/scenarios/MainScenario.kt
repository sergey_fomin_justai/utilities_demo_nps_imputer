package com.justai.jaicf.npsAfterCompletedRequestBot.scenarios

import com.justai.jaicf.activator.caila.caila
import com.justai.jaicf.builder.Scenario
import com.justai.jaicf.channel.jaicp.channels.TelephonyEvents
import com.justai.jaicf.channel.jaicp.dto.bargein.BargeInMode
import com.justai.jaicf.channel.jaicp.dto.bargein.BargeInTrigger
import com.justai.jaicf.channel.jaicp.dto.telephony
import com.justai.jaicf.channel.jaicp.reactions.telephony
import com.justai.jaicf.channel.jaicp.telephony
import com.justai.jaicf.npsAfterCompletedRequestBot.caila.CailaNumberParser
import com.justai.jaicf.npsAfterCompletedRequestBot.extensions.*
import com.justai.jaicf.npsAfterCompletedRequestBot.model.Imputer
import com.justai.jaicf.npsAfterCompletedRequestBot.scenarios.consts.*

// There are a lot of modal states in this scenario.
// The reason for that is because there are a few situations where we have to
// take certain user's input, although we have no idea what exactly that input might be,
// hence we cannot make intents to catch that input.


val mainScenario = Scenario(telephony) {

    state("/Greeting") {
        activators { regex("/start") }
        action {
            user.setDataFromPayload(request.calleePayload)

            // If for some reason there's no request data - end call
            if (user.isRequestEmpty()) {
                reactions.go("/End call and send reports"); return@action
            }

            Thread.sleep(500)  // To make a little pause
            reactions.bargeIn(BargeInMode.FORCED, BargeInTrigger.INTERIM, 0)
            reactions.audio(
                Imputer.getGreetingPhrase(user.gmtZone, companyName),
                "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/GREETING_ive_got_question_may_i_ask/GREETING_ive_got_question_may_i_ask.wav",
                bargeInContext = "/Auto answering phone"
            )

            Imputer.cache(
                listOf(
                    "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=CLOSE_you_had_req_its_done_can_i_close&speed=0.9&token=KaiPhei7rae1eeh",
                    "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=CLOSE_you_had_req_its_done_can_i_close_it&speed=0.9&token=KaiPhei7rae1eeh",
                    "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=REASON_there_was_your_req_may_i_ask&speed=0.9&token=KaiPhei7rae1eeh",
                    "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=REASON_wanna_ask_about_your_req_may_i&speed=0.9&token=KaiPhei7rae1eeh",
                    "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=REASON_wanna_ask_you_about_you_req_could_i&speed=0.9&token=KaiPhei7rae1eeh",
                    "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=REASON_you_have_req_may_i_ask&speed=0.9&token=KaiPhei7rae1eeh"
                )
            )
            saveAsLastState()
        }

    }

    state("/Say reason for calling") {
        activators {
            intent("What do you want?")
            intent("What request?")
        }
        activators("/Greeting") {
            intent("What do you want?")
            intent("What request?")
        }
        action {
            val phrase = random(
                "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=REASON_there_was_your_req_may_i_ask&speed=0.9&token=KaiPhei7rae1eeh",
                "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=REASON_wanna_ask_about_your_req_may_i&speed=0.9&token=KaiPhei7rae1eeh",
                "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=REASON_wanna_ask_you_about_you_req_could_i&speed=0.9&token=KaiPhei7rae1eeh",
                "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=REASON_you_have_req_may_i_ask&speed=0.9&token=KaiPhei7rae1eeh"
            )
            reactions.bargeIn(BargeInMode.FORCED, BargeInTrigger.FINAL, 0)
            reactions.audio(phrase, bargeIn = true)
            saveAsLastState()
        }
    }


    state("/Wait for reply") {
        activators { intent("Wait for a sec") }
        action { reactions.audio("https://248305.selcdn.ru/demo_bot_static/utilities-NPS/GENERAL_ok_good/GENERAL_ok_good.wav") }
    }


    state("/Wait what, it's done") {
        activators {
            intent("Request is already completed")
        }
        action {
            callResult.startCallStatus = ANSWERED_FIRST_QUESTION

            reactions.run {
                audio(
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/CLOSE_can_i_close/CLOSE_can_i_close.wav",
                    bargeIn = true
                )
                changeState("/You've had a request, close?")
            }
            saveAsLastState()
        }
    }


    state("/You've had a request, close?", modal = true) {
        append(offtopStates)
        append(commonStates)

        activators {
            intent("I'm listening")
        }
        activators("/Greeting") {
            intent("Hello")
            intent("I'm listening")
            intent("Yes")
        }
        activators("/Say reason for calling") {
            intent("Hello")
            intent("I'm listening")
            intent("Yes")
        }
        action {
            callResult.startCallStatus = ANSWERED_FIRST_QUESTION

            val phrase = random(
                "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=CLOSE_you_had_req_its_done_can_i_close&speed=0.9&token=KaiPhei7rae1eeh",
                "https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?task=${user.request?.requestName}&template_id=CLOSE_you_had_req_its_done_can_i_close_it&speed=0.9&token=KaiPhei7rae1eeh"
            )
            reactions.bargeIn(BargeInMode.FORCED, BargeInTrigger.FINAL, 0)
            reactions.audio(
                phrase,
                "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/CLOSE_can_i_close/CLOSE_can_i_close.wav",
                bargeIn = true
            )
            saveAsLastState()
        }

        state("No") {
            activators {
                intent("No")
                intent("No, don't close")
            }
            action {
                user.request?.requestStatus = "ОТКРЫТА ПОВТОРНО"
                reactions.go("/Why don't you wanna close")
            }
        }

        state("Yes") {
            activators {
                intent("Yes")
                intent("Yes, close")
                intent("Request is already completed")
            }
            action {
                user.request?.requestStatus = "ЗАКРЫТА"
                reactions.go("/Evaluate please")
            }
        }
        state("Extended sentence instead of yes or no") {
            activators { catchAll() }
            action {
                user.request?.setComment(request.input)

                val phrase = random(
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/CLOSE_i_see_but_again_can_i_close/CLOSE_i_see_but_again_can_i_close.wav",
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/CLOSE_i_see_but_can_i_close/CLOSE_i_see_but_can_i_close.wav"
                )
                reactions.audio(phrase)
                reactions.changeState("/You've had a request, close?")
            }
        }
    }


    state("/Why don't you wanna close", modal = true) {
        append(offtopStates)
        append(commonStates)

        action {
            val phrase = random(
                "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/WHYNOT_tell_us_why_dont_you_wanna_close/WHYNOT_tell_us_why_dont_you_wanna_close.wav",
                "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/WHYNOT_why_dont_you_wanna_close/WHYNOT_why_dont_you_wanna_close.wav"
            )
            reactions.audio(phrase, bargeIn = true)
            saveAsLastState()
        }

        state("No, no it's done") {
            activators { intent("No, no it's done") }
            action { reactions.go("/You've had a request, close?/Yes") }
        }

        state("Reason") {
            activators { catchAll() }
            action {
                user.request?.setComment(request.input)

                val phrase = random(
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/WHYNOT_i_opened_you_req_again/WHYNOT_i_opened_you_req_again.wav",
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/WHYNOT_i_see_opened_your_req/WHYNOT_i_see_opened_your_req.wav"
                )
                reactions.audio(phrase)
                reactions.go("/Thank you, bye!")
            }
        }
    }


    state("/Evaluate please", modal = true) {
        append(offtopStates)
        append(commonStates)

        action {
            reactions.audio(
                "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/EVAL_ok_please_evaluate/EVAL_ok_please_evaluate.wav",
                "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/EVAL_please_evaluate/EVAL_please_evaluate.wav",
                bargeIn = true
            )
            saveAsLastState()
        }

        state("We need it because of the reasons") {
            activators { intent("Why do you need that?") }
            action {
                val phrase = random(
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/EVAL_feedback_is_needed/EVAL_feedback_is_needed.wav",
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/EVAL_we_ask_for_feedback_to_improve/EVAL_we_ask_for_feedback_to_improve.wav"
                )
                reactions.run {
                    bargeIn(BargeInMode.FORCED, BargeInTrigger.FINAL, 0)
                    audio(phrase, bargeIn = true)
                    changeState("/Evaluate please")
                }
            }
        }

        state("User doesn't wanna evaluate") {
            activators {
                intent("I don't want to")
                intent("No")
            }
            action {
                user.request?.setComment("Жилец отказался давать оценку")
                reactions.audio("https://248305.selcdn.ru/demo_bot_static/utilities-NPS/GENERAL_ok_good/GENERAL_ok_good.wav")
                reactions.go("/Thank you, bye!")
            }
        }

        state("Evaluation") {
            activators {
                intent("Giving evaluation")
                catchAll()
            }
            action {
                val eval = activator.caila?.let { CailaNumberParser(it).getNumber() }

                if (eval != null && eval > 0.0 && eval <= 5.0) {
                    user.request?.evaluation = eval.toInt()
                    user.request?.setComment(request.input)
                    reactions.go("/What can we improve?")
                } else {
                    reactions.go("/Evaluate please/Ask for numeric evaluation")
                }
            }
        }

        state("Ask for numeric evaluation") {
            action {
                user.request?.setComment(request.input)
                reactions.bargeIn(BargeInMode.FORCED, BargeInTrigger.FINAL, 0)
                reactions.audio(
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/EVAL_i_see_but_can_you_give_num/EVAL_i_see_but_can_you_give_num.wav",
                    bargeIn = true
                )
                reactions.changeState("/Evaluate please")
            }
        }
    }


    state("/What can we improve?", modal = true) {
        append(offtopStates)
        append(commonStates)

        action {
            val phrase = random(
                "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/IMPROVE_what_can_we_improve/IMPROVE_what_can_we_improve.wav",
                "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/IMPROVE_what_could_we_improve/IMPROVE_what_could_we_improve.wav"
            )
            reactions.audio(phrase)
            saveAsLastState()
        }

        state("Commentary") {
            activators {
                intent("Giving evaluation")
                catchAll()
            }
            action {
                user.request?.setComment(request.input)
                reactions.audio("https://248305.selcdn.ru/demo_bot_static/utilities-NPS/GENERAL_ok_good/GENERAL_ok_good.wav")
                reactions.go("/Thank you, bye!")
            }
        }
    }


    state("/Can't talk") {
        activators {
            intent("Can't talk")
        }
        activators("/Greeting") {
            intent("No")
        }
        action {
            callResult.startCallStatus = CANT_TALK
            reactions.go("/Sorry, bye!")
        }
    }

    state("/Wrong number") {
        activators { intent("Wrong number") }
        action {
            callResult.startCallStatus = WRONG_NUMBER
            reactions.go("/Sorry, bye!")
        }
    }

    state("/Auto answering phone", modal = true) {
        state("Activator") {
            activators { intent("Auto answer") }
            action {
                callResult.startCallStatus = AUTO_ANSWER
                reactions.go("/End call and send reports")
            }
        }
    }

    state("/onCallNotConnected") {
        activators { event("onCallNotConnected") }
        action {
            user.setDataFromPayload(request.calleePayload)
            callResult.startCallStatus = DISCONNECT
            reactions.go("/End call and send reports")
        }
    }


    state("/User hang up") {
        globalActivators {
            event(TelephonyEvents.HANGUP)
            event(TelephonyEvents.NO_DTMF_ANSWER)
        }
        action {
            callResult.endCallStatus = DISCONNECT
            reactions.go("/End call and send reports")
        }
    }


    state("/Thank you, bye!") {
        action {
            reactions.run {
                audio("https://248305.selcdn.ru/demo_bot_static/utilities-NPS/BYE_thank_you_bye/BYE_thank_you_bye.wav")
                go("/End call and send reports")
            }
        }
    }


    state("/Sorry, bye!") {
        action {
            reactions.run {
                audio("https://248305.selcdn.ru/demo_bot_static/utilities-NPS/BYE_sorry_bye/BYE_sorry_bye.wav")
                go("/End call and send reports")
            }
        }
    }


    state("/NoMatch") {
        activators {
            catchAll()
        }
        action {
            if (app.catchAll.counter > 2) {
                reactions.go("/End call from fallback")
            } else {
                app.catchAll.counter++
                val phrase = random(
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/FALLBACK_didnt_really_get/FALLBACK_didnt_really_get.wav",
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/FALLBACK_didnt_understand/FALLBACK_didnt_understand.wav"
                )
                reactions.run {
                    audio(phrase)
                    goToPreviousState()
                }
            }
        }
    }


    state("/End call from fallback") {
        action {
            callResult.endCallStatus = OPERATOR
            reactions.run {
                audio("https://248305.selcdn.ru/demo_bot_static/utilities-NPS/FALLBACK_didnt_understand_call_you_back_bye/FALLBACK_didnt_understand_call_you_back_bye.wav")
                go("/End call and send reports")
            }
        }
    }

    state("/End call and send reports") {
        activators { intent("Goodbye") }
        action {
            if (app.isFinished) {
                reactions.go("/Bot hang up")
            } else {
                if (callResult.startCallStatus == null) { callResult.startCallStatus = DISCONNECT }

                val requestSuccessfullyClosed = user.request?.requestStatus == CLOSED && user.request?.isEvaluationEmpty() == false
                val requestIsOpenedAgain = user.request?.requestStatus == OPENED && user.request?.commentary != null

                if (callResult.endCallStatus != OPERATOR) {
                    callResult.endCallStatus = if (requestSuccessfullyClosed || requestIsOpenedAgain) {
                        SUCCESS
                    } else {
                        DISCONNECT
                    }
                }

                callResult.callResultUrl = sessionId?.let { request.getAudioUrl(it) }
                callResult.sendReport()                                                      // Report to company API
                setJaicpAnalytics()                                                          // Data for JAICP analytics
                reactions.run {
                    telephony?.makeReport(callResult, request.calleePayload.toString())      // Call campaign report
                    telephony?.hangupAndClear(app, callResult, user)                         // Hang up and clear this call's data

                    if (request.telephony?.input == "onCallNotConnected"
                        || request.telephony?.input == "hangup") {
                            go("/Bot hang up")
                    }
                }
            }
        }
    }

    // This state exists simply because when bot finishes and hangs up, TelephonyEvents.BOT_HANGUP happens and bot reacts to it.
    // Moreover it sometimes sends this fallback reaction phrase to a user as a recorded message.
    // And so, we process bot_hangup event and this state is the last step for each conversation, where each session ends.
    state("/Bot hang up") {
        globalActivators { event(TelephonyEvents.BOT_HANGUP) }
        action {
            app.isFinished = false
            context.cleanTempData()
            reactions.telephony?.endSession()
        }
    }
}