package com.justai.jaicf.npsAfterCompletedRequestBot.scenarios

import com.justai.jaicf.builder.Scenario
import com.justai.jaicf.channel.jaicp.telephony
import com.justai.jaicf.npsAfterCompletedRequestBot.extensions.companyName
import com.justai.jaicf.npsAfterCompletedRequestBot.extensions.goToPreviousState


val offtopStates = Scenario(telephony) {

    state("/What is your company?", noContext = true) {
        activators {
            intent("What is your company")
        }
        action {
            reactions.run {
                audio("https://imputer01-prod-sls.lab.just-ai.com/api/voice/tatiana/project/NPS_UprComp/imputer?company=${companyName.replace("\"", "").replace(" ", "%20")}&template_id=GREETING_company&speed=0.9&token=KaiPhei7rae1eeh")
                goToPreviousState()
            }
        }
    }


    state("/Are you a robot?", noContext = true) {
        activators {
            intent("Are you a robot?")
        }
        action {
            reactions.run {
                audio("https://248305.selcdn.ru/demo_bot_static/utilities-NPS/OFFTOP_i_am_alena_operator/OFFTOP_i_am_alena_operator.wav")
                goToPreviousState()
            }
        }
    }


    state("/What is your name?", noContext = true) {
        activators {
            intent("What is your name?")
        }
        action {
            val phrase = random(
                "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/OFFTOP_my_name_is_alena/OFFTOP_my_name_is_alena.wav",
                "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/OFFTOP_hm_alena/OFFTOP_hm_alena.wav"
            )
            reactions.run {
                audio(phrase)
                goToPreviousState()
            }
        }
    }

    state("/How can I contact you?", noContext = true) {
        activators {
            intent("How can I contact you?")
        }
        action {
            reactions.run {
                audio( "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/OFFTOP_you_can_call_us_or_visit_website/OFFTOP_you_can_call_us_or_visit_website.wav")
                goToPreviousState()
            }
        }
    }
}