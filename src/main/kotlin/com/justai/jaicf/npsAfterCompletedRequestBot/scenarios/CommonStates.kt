package com.justai.jaicf.npsAfterCompletedRequestBot.scenarios

import com.justai.jaicf.builder.Scenario
import com.justai.jaicf.channel.jaicp.channels.TelephonyEvents
import com.justai.jaicf.channel.jaicp.telephony
import com.justai.jaicf.npsAfterCompletedRequestBot.extensions.app
import com.justai.jaicf.npsAfterCompletedRequestBot.extensions.callResult
import com.justai.jaicf.npsAfterCompletedRequestBot.extensions.changeStateToPrevious
import com.justai.jaicf.npsAfterCompletedRequestBot.extensions.goToPreviousState
import com.justai.jaicf.npsAfterCompletedRequestBot.scenarios.consts.FallbackPhrases
import com.justai.jaicf.npsAfterCompletedRequestBot.scenarios.consts.OPERATOR

val commonStates = Scenario(telephony) {

    state("/Sudden hangup") {
        activators {
            event(TelephonyEvents.HANGUP)
            event(TelephonyEvents.BOT_HANGUP)
        }
        action { reactions.go("/User hang up") }
    }

    state("/NoInput") {
        activators {
            event(TelephonyEvents.SPEECH_NOT_RECOGNISED)
        }
        action {
            if (app.catchAll.counter >= 2) {
                reactions.go("/End call from fallback")
            } else {
                app.catchAll.counter++
                val phrase = random(
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/FALLBACK_couldnt_hear/FALLBACK_couldnt_hear.wav",
                    "https://248305.selcdn.ru/demo_bot_static/utilities-NPS/FALLBACK_couldnt_hear_you/FALLBACK_couldnt_hear_you.wav"
                )
                reactions.run {
                    audio(phrase)
                    goToPreviousState()
                }
            }
        }
    }

    state("/Send request to operator", noContext = true) {
        activators {
            intent("Ask operator")
        }
        action {
            if (app.askOperator.counter < 2) {
                app.askOperator.counter++
                reactions.run {
                    audio("https://248305.selcdn.ru/demo_bot_static/utilities-NPS/OFFTOP_no_operator_will_try_to_help/OFFTOP_no_operator_will_try_to_help.wav")
                    goToPreviousState()
                }
            } else {
                callResult.endCallStatus = OPERATOR
                reactions. run {
                    audio("https://248305.selcdn.ru/demo_bot_static/utilities-NPS/OFFTOP_operator_will_call_you_bye/OFFTOP_operator_will_call_you_bye.wav")
                    go("/End call and send reports")
                }
            }
        }
    }

    state("Repeat the last phrase", noContext = true) {
        activators {
            intent("Repeat")
        }
        action {
            val lastPhrase = context.session["lastPhrase"] as String
            if (lastPhrase in FallbackPhrases) {
                reactions.goToPreviousState()
            } else {
                reactions.audio(lastPhrase)
                reactions.changeStateToPrevious()
            }
        }
    }
}