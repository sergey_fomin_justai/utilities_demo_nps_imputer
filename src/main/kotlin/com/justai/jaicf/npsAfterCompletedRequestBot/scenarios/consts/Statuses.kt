package com.justai.jaicf.npsAfterCompletedRequestBot.scenarios.consts

// Call result statuses
const val AUTO_ANSWER = "АВТООТВЕТЧИК"
const val DISCONNECT = "СБРОС ЗВОНКА"
const val CANT_TALK = "НЕТ ВОЗМОЖНОСТИ ГОВОРИТЬ"
const val WRONG_NUMBER = "ОШИБЛИСЬ НОМЕРОМ"
const val OPERATOR = "ТРЕБУЕТСЯ ОПЕРАТОР"
const val ANSWERED_FIRST_QUESTION = "ОТВЕТИЛ НА ПЕРВЫЙ ВОПРОС"
const val SUCCESS = "ЗАВЕРШЕННЫЙ ВЫЗОВ"

// Request statuses
const val CLOSED = "ЗАКРЫТА"
const val OPENED = "ОТКРЫТА ПОВТОРНО"